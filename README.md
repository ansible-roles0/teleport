Teleport
=========

This role lets you install and initialize Teleport on a Linux server based on RedHad

Features
--------

- Install teleport

Requirements
------------

- OS based on RedHad for target

Role Variables
--------------

Variable | Default value |Description
---------|---------------|--------------
teleport_version   | 15.3.6   | Version of teleport 
teleport_service   | teleport | Service name
cluster_name   | teleport.example.com | Name of the cluster teleport
public_addr    | {{cluster_name}}:443 | Public address and listen port
datadir   | /var/lib/teleport | Data directory
bindir    | /usr/local/bin    | Binary directory
cert_file | /etc/pki/tls/certs/fullchain.pem | Path of certificate file
key_file  | /etc/pki/tls/private/privkey.pem | Path of key file
admin_username | teleport-admin | Username of the fisrt teleport administrator
userlogins | [ ubuntu, ec2-user] | List of teleport users target accounts
adminlogins | [root, ubuntu, ec2-user ] | List of {{admin_username}} administrator target accounts


Dependencies
------------

None

Example Playbook
----------------


```yml
---
- name: Playbook Teleport
  hosts: all
  roles:
    - role: teleport
      cluster_name: teleport.opensys.lab
      teleport_adminlogins:
        - root
        - opensys
        - ubuntu

```

License
-------

BSD

Author Information
------------------

*Created at :* 24/05/2024
